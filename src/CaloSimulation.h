#ifndef CaloSimulation_h
#define CaloSimulation_h

/**
 * CaloSimulation
 * Implements the simulation of the calorimter.
 */

#include "CellAddress.h"
#include "CaloCell.h"
#include <vector>
#include <map>

class CaloSimulation
{
public:
	// Constuctor
	CaloSimulation();

	// Destructor
	virtual ~CaloSimulation();

	// Add the calorimeter cells to the vector of cell caldata.
	void CalorimeterData(std::vector<CaloCell>& caldata) ;

	// Simulate a shower of a given energy, starting from the impact point (x,y)
	// of the electron at the front end of the calorimeter.
	void SimulateShower(float x, float y, float energy);

	// Simulate a shower of a given energy, starting from the impact point (x,y)
	// of a hadron at the front end of the calorimeter.
	void SimulateHadShower(float x, float y, float energy);

	// Print all the cells.
	friend std::ostream& operator<<(std::ostream& os, const CaloSimulation& cs);

	// getters
	std::vector<CaloCell> getCalData() const { return m_caldata; }

private:
	std::vector<CaloCell> m_caldata;
};

#endif
