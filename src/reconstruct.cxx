#include <cmath>
#include "TRandom3.h"
#include "TH1D.h"
#include "TF1.h"
#include "TMath.h"

#include "Event.h"
#include "CaloGeometry.h"
#include "CaloConstants.h"

using namespace std;
using namespace CalConst;
using namespace TMath;


void reconstruct(Event& event)
{
  TRandom3 *rand = new TRandom3(0);
  rand->SetSeed(0);
  float e = event.eTrue(); // get true energy
  float eReso = 0.1 * sqrt(e); // compute resolution
  // float eSmeared = e + eReso * (rand->Rndm() - 0.5); // smear with a flat distribution
  float eSmeared = e + eReso * (rand->Gaus()); // smear with a gaussian distribution
  // float eSmeared = rand->Gaus(e, eReso); // alternate approach to do the same?
  event.seteReco(eSmeared); // set the reconstructed energy
  event.seteRecoBias(eSmeared+0.1); // set the biased reconstructed energy

  // Initialize histograms
  char const* x_name = ("x_"+to_string(event.eventNumber())).c_str();
  TH1D * x_histo = new TH1D(x_name, x_name, NbCellsInXY, XYMin, XYMax);

  char const* y_name = ("y_"+to_string(event.eventNumber())).c_str();
  TH1D * y_histo = new TH1D(y_name, y_name, NbCellsInXY, XYMin, XYMax);

  float x_RecoW = 0.;
  float y_RecoW = 0.;
  float e_sum_log  = 0.;
  float e_temp = 0;
  // fill histogram to find reconstructed deposition
  // compute weighted mean for x,y to find weighted reconstructed deposition
  for (size_t i_c = 0; i_c < event.caldata().size(); i_c++) {
    CellAddress cl_temp = event.caldata()[i_c].address();
    e_temp = event.caldata()[i_c].energy();
    if (e_temp > 1e-12){
      x_RecoW += CaloGeometry::xCentre(cl_temp) * Power(e_temp, alpha) * Power(Log(e_temp), beta);
      y_RecoW += CaloGeometry::yCentre(cl_temp) * Power(e_temp, alpha) * Power(Log(e_temp), beta);
      e_sum_log  += Power(e_temp, alpha) * Power(Log(e_temp), beta);

      x_histo->Fill(CaloGeometry::xCentre(cl_temp), e_temp);
      y_histo->Fill(CaloGeometry::yCentre(cl_temp), e_temp);
    }
  }

  // fit histogram: mean - reconstructed position (xReco); 2*width = resolution (xWidth)
  x_histo->Fit("gaus", "LQ");
  TF1* x_fit = x_histo->GetFunction("gaus");
  event.setxReco(x_fit->GetParameter(1));
  event.setxRecoW(x_RecoW/e_sum_log);
  event.setxWidth(2 * x_fit->GetParameter(2));

  // fit histogram: mean - reconstructed position (yReco); 2*width = resolution (yWidth)
  y_histo->Fit("gaus", "LQ");
  TF1 *y_fit = y_histo->GetFunction("gaus");
  event.setyReco(y_fit->GetParameter(1));
  event.setyRecoW(y_RecoW/e_sum_log);
  event.setyWidth(2 * y_fit->GetParameter(2));

  // draw x and y distribytion only for 0 (first) event
  if (event.eventNumber() == 0){
    x_histo->Draw();
    y_histo->Draw();
  }
  else
  {
    delete x_histo;
    delete y_histo;
  }
  delete rand;
}
