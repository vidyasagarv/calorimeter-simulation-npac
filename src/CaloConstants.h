#ifndef CaloConstants_h
#define CaloConstants_h

/**
 * CaloConstants
 * Defines constants used across many files of the project.
 */

namespace CalConst {
	// Parameters for electromagnetic shower simulation.
	static const float a_em = 4.0;
	static const float b_em = 0.5;
	static const float X0_em = 1e-2; //[m]
  static const float MR_em = 5e-2; //[m]

	// Parameters for purely hadronic shower
	static const float a_had = 2.;
	static const float b_had = 1.;
	static const float L_had = 10e-2; //[m]
	static const float MR_had = 10e-2; //[m]

	// Parameters for x/y reconstruction
	static const float alpha = 0.5;
	static const float beta  = 0.;

	// Dimensions, in meters.
	static const float ZMin = 0.;
	static const float ZMax = 1.;
	static const float XYMin = -2.;
	static const float XYMax = 2.;

	static const int NbCellsInXY = 40;
	static const int NbLayers = 10;

	// Cell size in x-y.
	static const float XYSize = (XYMax - XYMin) / NbCellsInXY;
	static const float ZSize = (ZMax - ZMin) / NbLayers;
}

#endif
