#include "Event.h"
#include "CaloSimulation.h"
#include "CaloConstants.h"
#include "TRandom3.h"

using namespace CalConst;

void simulate(Event& event)
{
    // simulate the event
    CaloSimulation sim;

    TRandom3 *rand = new TRandom3(0);
    rand->SetSeed(0);
    float x_int = rand->Uniform(XYMin,XYMax);
    float y_int = rand->Uniform(XYMin,XYMax);
    // float x_int = rand->Uniform(-0.1,0);
    // float y_int = rand->Uniform(-0.1,0);
    event.setxTrue(x_int);
    event.setyTrue(y_int);

    float trueEnergy = 50.;

    if (event.eventNumber() < 200) { // First half of the events will be electron showers
      sim.SimulateShower(x_int, y_int, trueEnergy);
      event.setType(0);
    }
    else {
      sim.SimulateHadShower(x_int, y_int, trueEnergy);
      event.setType(1);
    }

    event.setCalData(sim.getCalData());

    event.seteTrue(trueEnergy); // fixed true energy

    delete rand;
}
