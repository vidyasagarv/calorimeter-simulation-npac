#include "TH1F.h"
#include "TRandom.h"

void genGauss()
{
    TH1F* h = new TH1F("hGauss", "Gaussian distribution", 100, -2., 2.);
    TH1F* hDir = new TH1F("hGaussDir", "Direct Gaussian distribution", 100, -2., 2.);

    float mean  = 0.0;
    float sigma = 0.5;
    float g_max = 1.0;
    float area  = 0.0;
    int tot_count = 10000;

    TF1 *fg = new TF1("fg","gaus", -2, 2);
    fg->SetParameter(0, g_max);
    fg->SetParameter(1, mean);
    fg->SetParameter(2, sigma);
    // Draw 10000 numbers according the gaussian law
    // We use gRandom->Uniform(MAX)  to draw numbers
    // uniformly  between  0. and MAX
    for (int i=0; i<tot_count; i++) {
        float x = 0.;
        float y = 0.;
        // put here your piece of code
        x = gRandom->Uniform(-2.,2.);
        y = gRandom->Uniform(1);
        if (y < fg->Eval(x)){
          h->Fill(x);         // we  fill here the histogram
          hDir->Fill(gRandom->Gaus(0,0.5));
          if(TMath::Abs(x) > 3*sigma)
            area+=1;
        }
    }

    std::cout<<"Area outside 3*sigma = "<<area/tot_count<<std::endl;

    hDir->SetLineColor(kBlack);
    hDir->SetFillColor(kRed);
    hDir->SetFillStyle(3001);

    h->SetLineColor(kBlue);
    h->SetFillColor(kBlue);
    h->SetFillStyle(3001);

    auto cGaus = new TCanvas("cGaus","cGaus");
    h->Draw("");
    hDir->Draw("SAME");
    h->Fit(fg);
    fg->Draw("SAME");

    auto legend = new TLegend(0.1,0.7,0.48,0.9);
    legend->SetHeader("The Gaussian distribution","C"); // option "C" allows to center the header
    legend->AddEntry(h,"MC","f");
    legend->AddEntry(hDir,"Randomizer","f");
    legend->AddEntry(fg,"Gauss fit of MC","l");
    legend->Draw();

}
