/**
 * CaloCell
 * Reprensents a cell of the calorimeter, and its energy.
 */

#include "CellAddress.h"
#include "CaloCell.h"

// Default constructor needed of containers of the standard template library.
CaloCell::CaloCell()
{
}

// Full constructor.
CaloCell::CaloCell(const CellAddress& ca, float energy)
{
  m_address = ca;
  m_energy  = energy;
}

// Destructor
CaloCell::~CaloCell()
{
}

// Accessors.
float CaloCell::energy() const { return m_energy; }
CellAddress CaloCell::address() const { return m_address; }

// Setters.
void CaloCell::setAddress(CellAddress address) { m_address = address;}
void CaloCell::setEnergy(float energy) { m_energy = energy;}
