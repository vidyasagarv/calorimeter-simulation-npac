/**
 * CaloSimulation
 * Implements the simulation of the calorimter.
 */

#include "CaloSimulation.h"
#include "CaloConstants.h"
#include "CaloGeometry.h"
#include "CaloCell.h"
#include "TF1.h"
#include "TF2.h"
#include "TRandom3.h"
#include "TMath.h"

using namespace CalConst;

typedef std::map<CellAddress,float> Cells;

// Constructor
CaloSimulation::CaloSimulation()
{
  for (size_t i_z = 0; i_z < NbLayers; i_z++) {
    for (size_t i_y = 0; i_y < NbCellsInXY; i_y++) {
      for (size_t i_x = 0; i_x < NbCellsInXY; i_x++) {
        CellAddress cell_ad(i_x,i_y,i_z);
        CaloCell empty_cell(cell_ad, 0.);
        m_caldata.push_back(empty_cell);
      }
    }
  }
}

// Destructor
CaloSimulation::~CaloSimulation()
{
}

// Add the calorimeter cells to the vector of cell caldata.
void CaloSimulation::CalorimeterData(std::vector<CaloCell>& caldata)
{
  // Create empty cells map
  Cells cells;

  //  cells at beginning: fill map with initial calorimeter info
  for (std::vector<CaloCell>::iterator it_calo = m_caldata.begin(); it_calo != m_caldata.end(); ++it_calo) {
    cells[it_calo->address()] = it_calo->energy();
  }

  // update calorimter map with non-zero cells
  for (std::vector<CaloCell>::iterator it_calo_new = caldata.begin(); it_calo_new != caldata.end(); it_calo_new++) {
    Cells::iterator pos = cells.find(it_calo_new->address());
    if ( pos != cells.end() ) {     // Cell found. Add energy deposit
      cells[it_calo_new->address()] =  pos->second + it_calo_new->energy();
    }
    else{                           // Cell not found
      std::cout<<"WARNING: Cell is outside the calorimeter"<<std::endl;
    }
  }

  // update calorimter status with a shower
  for (std::vector<CaloCell>::iterator it_calo = m_caldata.begin(); it_calo != m_caldata.end(); it_calo++) {
    it_calo->setEnergy(cells[it_calo->address()]);
  }
}

// Simulate a shower of a given energy, starting from the impact point (x,y)
// of the electron at the front end of the calorimeter.
void CaloSimulation::SimulateShower(float x, float y, float energy)
{
  // float energy_tot  = 0.;
  float energy_loss = 0.;
  float dE_dT_vol   = 0.;
  float t_front = 0.;
  float t_back  = 0.;
  float xl = 0.;
  float xh = 0.;
  float yl = 0.;
  float yh = 0.;

  float sigmaE = 0.1 * TMath::Power(energy/1., 0.5); // sigma(E) = 10% * sqrt(E/E0), where E0=1GeV
  std::vector<CaloCell> cell_vector;

  // TF1 *dE_dz = new TF1("dE_dz","[0]*[2]/6.*TMath::Power([2]*x,([1]-1))*TMath::Exp(-[2]*x)", ZMin, ZMax);
  TF1 *dE_dz = new TF1("dE_dz","[0]*TMath::GammaDist(x,[1],0.,1./[2])", ZMin, ZMax);
  dE_dz->SetParameters(energy, a_em, b_em);

  TF2 *dE_dT = new TF2("dE_dT","[0]/(2*TMath::Pi()*[2]*[4])*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4])", XYMin, XYMax, XYMin, XYMax);
  dE_dT->SetParameter(1,x);
  dE_dT->SetParameter(2,MR_em);
  dE_dT->SetParameter(3,y);
  dE_dT->SetParameter(4,MR_em);


  for (size_t i_z = 0; i_z < NbLayers; i_z++) {
    if (energy > 0.075){                  // ~0.075 GeV energy deposited in cell 5*MR_em away from (x,y)

      t_front = (i_z + 1.0)*ZSize / X0_em;
      t_back  =   i_z      *ZSize / X0_em;
      dE_dT_vol = dE_dz->Integral(t_back, t_front);

      if (energy-dE_dT_vol >= sigmaE)
      energy -= dE_dT_vol;
      else {
        dE_dT_vol = energy;
        energy = 0;
      }
      dE_dT->SetParameter(0,dE_dT_vol);

      for (size_t i_y = 0; i_y < NbCellsInXY; i_y++) {
        for (size_t i_x = 0; i_x < NbCellsInXY; i_x++) {
          xl = i_x*XYSize+XYMin;
          xh = (i_x+1)*XYSize+XYMin;
          yl = i_y*XYSize+XYMin;
          yh = (i_y+1)*XYSize+XYMin;
          energy_loss = dE_dT->Integral(xl,xh,yl,yh);
          // energy_loss = energy_loss + 0.1 * TMath::Power(energy_loss/1., 0.5)*gRandom->Gaus();
          energy_loss = energy_loss;
          CellAddress cell_ad(i_x,i_y,i_z);
          CaloCell cl(cell_ad, energy_loss);
          cell_vector.push_back(cl);
        }
      }

    }
    else
      break;
  }
  CalorimeterData(cell_vector);

}

// Simulate a shower of a given energy, starting from the impact point (x,y)
// of a hadron at the front end of the calorimeter.
void CaloSimulation::SimulateHadShower(float x, float y, float energy)
{
  TRandom3 *rand = new TRandom3(0);
  rand->SetSeed(0);
  float f = rand->Uniform(0,1); // fraction of the hadronic component
  // std::cout << "f = " << f << '\n';

  float energy_loss = 0.;
  float dE_dT_vol_em = 0.;
  float dE_dT_vol_had = 0.;
  float dE_dT_vol = 0.;
  float t_front = 0.;
  float t_back  = 0.;
  float xl = 0.;
  float xh = 0.;
  float yl = 0.;
  float yh = 0.;

  float sigmaE = 0.1 * TMath::Power(energy/1., 0.5); // sigma(E) = 10% * sqrt(E/E0), where E0=1GeV
  std::vector<CaloCell> cell_vector;

  // Longitudinal energy loss:
  // TF1 *dE_dz_em = new TF1("dE_dz","[0]*[2]/6.*TMath::Power([2]*x,([1]-1))*TMath::Exp(-[2]*x)", ZMin, ZMax);
  TF1 *dE_dz_em = new TF1("dE_dz","[0]*TMath::GammaDist(x,[1],0.,1./[2])", ZMin, ZMax);
  dE_dz_em->SetParameters(energy, a_em, b_em);

  // TF1 *dE_dz_had = new TF1("dE_dz","[0]*[2]/1.*TMath::Power([2]*x,([1]-1))*TMath::Exp(-[2]*x)", ZMin, ZMax);
  TF1 *dE_dz_had = new TF1("dE_dz","[0]*TMath::GammaDist(x,[1],0.,1./[2])", ZMin, ZMax);
  dE_dz_had->SetParameters(energy, a_had, b_had);

  // Transverse shower distributions:
  TF2 *dE_dT_em = new TF2("dE_dT","[0]/(2*TMath::Pi()*[2]*[4])*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4])", XYMin, XYMax, XYMin, XYMax);
  dE_dT_em->SetParameter(1,x);
  dE_dT_em->SetParameter(2,MR_em);
  dE_dT_em->SetParameter(3,y);
  dE_dT_em->SetParameter(4,MR_em);

  TF2 *dE_dT_had = new TF2("dE_dT","[0]/(2*TMath::Pi()*[2]*[4])*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4])", XYMin, XYMax, XYMin, XYMax);
  dE_dT_had->SetParameter(1,x);
  dE_dT_had->SetParameter(2,MR_had);
  dE_dT_had->SetParameter(3,y);
  dE_dT_had->SetParameter(4,MR_had);

  for (size_t i_z = 0; i_z < NbLayers; i_z++) {
    if (energy > 0.075){                  // ~0.075 GeV energy deposited in cell 5*MR_em away from (x,y)
      t_front = (i_z + 1.0)*ZSize / X0_em;
      t_back  =   i_z      *ZSize / X0_em;
      dE_dT_vol_em = dE_dz_em->Integral(t_back, t_front);
      dE_dT_vol_had = dE_dz_had->Integral(t_back, t_front);
      dE_dT_vol = (1.-f) * dE_dT_vol_em + f * dE_dT_vol_had;

      if (energy-dE_dT_vol >= sigmaE)
      energy -= dE_dT_vol;
      else {
        dE_dT_vol = energy;
        energy = 0;
      }
      dE_dT_em->SetParameter(0,(1.-f)*dE_dT_vol_em);
      dE_dT_had->SetParameter(0,f*dE_dT_vol_had);

      for (size_t i_y = 0; i_y < NbCellsInXY; i_y++) {
        for (size_t i_x = 0; i_x < NbCellsInXY; i_x++) {
          xl = i_x*XYSize+XYMin;
          xh = (i_x+1)*XYSize+XYMin;
          yl = i_y*XYSize+XYMin;
          yh = (i_y+1)*XYSize+XYMin;
          energy_loss = dE_dT_em->Integral(xl,xh,yl,yh) + dE_dT_had->Integral(xl,xh,yl,yh);
          CellAddress cell_ad(i_x,i_y,i_z);
          CaloCell cl(cell_ad, energy_loss);
          cell_vector.push_back(cl);
        }
      }

    }
    else
      break;
  }
  // Run CalorimeterData on the cell_vector to merge same cells
  CalorimeterData(cell_vector);
}
