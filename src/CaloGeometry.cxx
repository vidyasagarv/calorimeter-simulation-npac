/**
 * CaloGeometry
 * Helpers to describe the geometry of the detector.
 */

#include "CaloGeometry.h"
#include "CellAddress.h"
#include "CaloConstants.h"

using namespace CalConst;

// Return false if the (x,y,z) point is not located in the calorimeter
// volume otherwise return true and fill the CellAddress variables with the
// address of the cell that contains this point.
bool CaloGeometry::IsInside(double xyz[3], CellAddress& cellAddress)
{
  int x_coord = int((xyz[0]-XYMin)/XYSize);
  int y_coord = int((xyz[1]-XYMin)/XYSize);
  int z_coord = int((xyz[2]-ZMin)/ZSize);
  CellAddress temp_address(x_coord, y_coord, z_coord);
  if (temp_address.IsValid()){
    cellAddress = temp_address;
    return true;
  }
  else
    return false;
}

// Give the position of the cell center.
double CaloGeometry::xCentre(const CellAddress& cellAddress)
{
  return (cellAddress.ix() + 0.5)*XYSize + XYMin;
}

double CaloGeometry::yCentre(const CellAddress& cellAddress)
{
  return (cellAddress.iy() + 0.5)*XYSize + XYMin;
}
