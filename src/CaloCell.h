#ifndef CaloCell_h
#define CaloCell_h

/**
 * CaloCell
 * Reprensents a cell of the calorimeter, and its energy.
 */

#include "CellAddress.h"

class CaloCell
{
public:

	// Default constructor needed of containers of the standard template library.
	CaloCell();

	// Full constructor.
	CaloCell(const CellAddress& ca, float energy);

	// Destructor
	virtual ~CaloCell();

	// Accessors.
	float energy() const;
	CellAddress address() const;

	// Setters.
	void setEnergy(float energy);
	void setAddress(CellAddress address);

	// "Print" function.
	friend std::ostream& operator<<(std::ostream& os, const CaloCell& y)
	{
		os << "[" << y.address() << ", " << y.energy() << "]";
		return os;
	}

private:
	float m_energy;
	CellAddress m_address;
};

#endif
