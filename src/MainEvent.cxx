////////////////////////////////////////////////////////////////////////
//
//             A simple example with a ROOT tree
//             =================================
//
//  This program creates :
//    - a ROOT file
//    - a tree
// One possible argument : number of events
//   ---Running/Linking instructions----
//  This program consists of the following files and procedures.
//    - Event.h event class description
//    - Event.C event class implementation
//    - MainEvent.C the main program to demo this class might be used (this file)
//
//   ---Analyzing the event.root file with the interactive root
//        example of a simple session
//   Root > TFile f("Event.root")
//   Root > eventTree.Draw("eTrue") // histogram true x
//   Root > eventTree.Draw("eReco:eTrue", "eventNumber<10") // histogram E reco vs E true for first 10 events
//
////////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include <iostream>

#include "TFile.h"
#include "TRandom3.h"
#include "TTree.h"
#include "TH3D.h"
#include "TF1.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TStopwatch.h"

#include "Event.h"
#include "CellAddress.h"
#include "CaloConstants.h"
#include "CaloCell.h"
#include "CaloGeometry.h"

using namespace std;
using namespace CalConst;

void reconstruct(Event& event);
void simulate(Event& event);
void ana_simu(const Event& event);

//______________________________________________________________________________
int main(int argc, char **argv)
{
    // By default create 400 events.
    int nEventsMax = 400;
    // If command line argument provided, take it as max number of events.
    if (argc > 1) nEventsMax = atoi(argv[1]);

    // Create a new ROOT binary machine independent file.
    // Note that this file may contain any kind of ROOT objects, histograms,
    // pictures, graphics objects, detector geometries, tracks, events, etc..
    // This file is now becoming the current directory.
    TFile outFile("Event.root", "RECREATE");

    // Create a ROOT Tree and the branches to be written.
    // The instruction "Branch" tells ROOT that the value stored in this variable
    // should be written in the tree when the TTree::Fill() method is called.
    // The first arg of Branch is the name given in the Tree, the second is the
    // address of the variable to be written.
    // !!! You should add similar groups of lines if you want to store other
    // variables in the output tree.
    TTree outTree("eventTree", "Calo sim root tree");
    int eventNumber;
    float eTrue;
    float eReco;
    float eRecoBias;
    float xTrue;
    float yTrue;
    float xReco;
    float yReco;
    float xRecoW;
    float yRecoW;
    float xWidth_em, xWidth_had;
    float yWidth_em, yWidth_had;

    outTree.Branch("eventNumber", &eventNumber);
    outTree.Branch("eTrue", &eTrue);
    outTree.Branch("eReco", &eReco);
    outTree.Branch("eRecoBias", &eRecoBias);
    outTree.Branch("xTrue", &xTrue);
    outTree.Branch("yTrue", &yTrue);
    outTree.Branch("xReco", &xReco);
    outTree.Branch("yReco", &yReco);
    outTree.Branch("xRecoW", &xRecoW);
    outTree.Branch("yRecoW", &yRecoW);

    TTree eShower("eShower", "Discriminating variable of electron shower");
    eShower.Branch("xWidth", &xWidth_em);
    eShower.Branch("yWidth", &yWidth_em);

    TTree hadShower("hadShower", "Discriminating variable of hadron shower");
    hadShower.Branch("xWidth", &xWidth_had);
    hadShower.Branch("yWidth", &yWidth_had);
    // Create a dummy event that will be build in the loop.
    Event event;

    // Loop over the events.
    for (eventNumber = 0; eventNumber < nEventsMax; eventNumber++) {
        if (eventNumber % 100 == 0) {
            // Just to check everything is going smoothly.
            cout << "Doing event: " << eventNumber << endl;
        }

        // initialize event
        event.build(eventNumber);

        // simulation
        simulate(event);
        ana_simu(event);

        // reconstruction
        reconstruct(event);

        // Prepare to fill the output tree.
        eTrue = event.eTrue();
        eReco = event.eReco();
        eRecoBias = event.eRecoBias();
        xTrue = event.xTrue();
        yTrue = event.yTrue();
        xReco = event.xReco();
        yReco = event.yReco();
        xRecoW = event.xRecoW();
        yRecoW = event.yRecoW();
        if (event.type() == 0){
          xWidth_em = event.xWidth();
          yWidth_em = event.yWidth();
          eShower.Fill();
        }
        else if (event.type() == 1){
          xWidth_had = event.xWidth();
          yWidth_had = event.yWidth();
          hadShower.Fill();
        }

        outTree.Fill(); // Fill the tree.
    } // End event loop

    outTree.Draw("(eReco-eTrue)>>hReso(240, -6., 6.)");           // Draw histo of resolution
    outTree.Draw("(eRecoBias-eTrue)>>hResoBias(240, -6., 6.)");   // Draw histo of biased resolution

    // Draw a Reco-True distribution
    outTree.Draw("(xReco-xTrue)>>hSshape_x(100,-0.005,0.005)");
    outTree.Draw("(yReco-yTrue)>>hSshape_y(100,-0.005,0.005)");

    // Fit Reco-True distribution to find resolution
    TH1D* hSshape_x = (TH1D*) gDirectory->Get("hSshape_x");
    TH1D* hSshape_y = (TH1D*) gDirectory->Get("hSshape_y");
    hSshape_x->Fit("gaus");
    hSshape_y->Fit("gaus");

    // Draw without displaying to extract vectors from TTree
    int n = outTree.Draw("xReco:(xReco-xTrue):yReco:(yReco-yTrue)","","goff");
    printf("The arrays' dimension is %d\n",n);

    // Retrieve variables 0, 1, 2 et 3
    double *vxReco   = outTree.GetVal(0);
    double *vxSshape = outTree.GetVal(1);
    double *vyReco   = outTree.GetVal(2);
    double *vySshape = outTree.GetVal(3);
    double *vxSshape_corr = outTree.GetVal(1);
    double *vySshape_corr = outTree.GetVal(3);

    // Create and draw graphs
    TGraph *gSshape_x = new TGraph(n,vxReco,vxSshape);
    TGraph *gSshape_y = new TGraph(n,vyReco,vySshape);
    gSshape_x->SetTitle("gSshape_x");
    gSshape_y->SetTitle("gSshape_y");
    gSshape_x->Draw("ap");
    gSshape_y->Draw("ap");

    // Fit function for further corrections
    TF1 *fx = new TF1("fx","pol3",-0.1,0);
    TF1 *fy = new TF1("fy","pol3",-0.1,0);
    fx->SetParameters(3e-4, 60, 1);
    fy->SetParameters(3e-4, 60, 1);
    gSshape_x->Fit("fx","R");
    gSshape_y->Fit("fy","R");


    // Histograms for updated Reco-True distributions
    TH1D* hSshape_x_corr = new TH1D("hSshape_x_corr", "x S-shape corrected", 100, -0.001, 0.001);
    TH1D* hSshape_y_corr = new TH1D("hSshape_y_corr", "y S-shape corrected", 100, -0.001, 0.001);

    // Correct S-shqpe distribution with fitted function
    for (int i_ev = 0; i_ev < n; i_ev++) {
      vxSshape_corr[i_ev] = vxSshape[i_ev] - fx->Eval(vxReco[i_ev]);
      vySshape_corr[i_ev] = vySshape[i_ev] - fy->Eval(vyReco[i_ev]);
      hSshape_x_corr->Fill(vxSshape_corr[i_ev]);
      hSshape_y_corr->Fill(vySshape_corr[i_ev]);
    }

    // Create and draw graphs
    TGraph *gSshape_x_corr = new TGraph(n,vxReco,vxSshape_corr);
    gSshape_x_corr->Draw("ap");
    TGraph *gSshape_y_corr = new TGraph(n,vyReco,vySshape_corr);
    gSshape_y_corr->Draw("ap");

    hSshape_x_corr->Fit("gaus");
    hSshape_x_corr->Draw();
    hSshape_y_corr->Fit("gaus");
    hSshape_y_corr->Draw();

    gSshape_x->Write();
    gSshape_y->Write();
    gSshape_x_corr->Write();
    gSshape_y_corr->Write();

    outFile.cd(); // Make sure we are in the outupt file.
    outFile.Write(); // Write all current in the current directory.

    outFile.Close();

    return 0;
}
