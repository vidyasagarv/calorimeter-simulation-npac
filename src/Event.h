#ifndef Event_h
#define Event_h

#include "CaloCell.h"
#include <vector>
/**
 * Event
 * Describes an event.
 */

class Event
{
private:
    int       m_eventNumber ; // event number
    float     m_eTrue ;       // true energy
    float     m_eReco ;       // reconstructed energy
    float     m_eRecoBias ;   // biased reconstructed energy
    float     m_xTrue ;       // true x of impact point
    float     m_yTrue ;       // true y of impact point
    float     m_xReco ;       // reconstructed x of impact point
    float     m_yReco ;       // reconstructed y of impact point
    float     m_xRecoW ;      // weighted reconstructed x of impact point
    float     m_yRecoW ;      // weighted reconstructed y of impact point
    float     m_xWidth ;      // width of the shower in x-direction
    float     m_yWidth ;      // width of the shower in y-direction
    std::vector<CaloCell> m_caldata;  // calorimter energy deposition
    int       m_type ;        // type of the shower: 0=electron shower; 1=hadron shower
public:
    Event(); // constructor
    virtual ~Event(); // destructor

    void build(int eventNumber); // initialize

    // setters
    void setEventNumber(int eventNumber) { m_eventNumber = eventNumber; }
    void seteTrue(float eTrue) { m_eTrue = eTrue; }
    void seteReco(float eReco) { m_eReco = eReco; }
    void seteRecoBias(float eRecoBias) { m_eRecoBias = eRecoBias; }
    void setxTrue(float x) { m_xTrue = x; }
    void setyTrue(float y) { m_yTrue = y; }
    void setxReco(float x) { m_xReco = x; }
    void setyReco(float y) { m_yReco = y; }
    void setxRecoW(float x) { m_xRecoW = x; }
    void setyRecoW(float y) { m_yRecoW = y; }
    void setxWidth(float x_w) { m_xWidth = x_w; }
    void setyWidth(float y_w) { m_yWidth = y_w; }
    void setCalData(std::vector<CaloCell> caldata) { m_caldata = caldata; }
    void setType(int type) { m_type = type; }

    // getters
    int eventNumber() const { return m_eventNumber; }
    float eTrue() const { return m_eTrue; }
    float eReco() const { return m_eReco; }
    float eRecoBias() const { return m_eRecoBias; }
    float xTrue() const { return m_xTrue; }
    float yTrue() const { return m_yTrue; }
    float xReco() const { return m_xReco; }
    float yReco() const { return m_yReco; }
    float xRecoW() const { return m_xRecoW; }
    float yRecoW() const { return m_yRecoW; }
    float xWidth() const { return m_xWidth; }
    float yWidth() const { return m_yWidth; }
    std::vector<CaloCell> caldata() const { return m_caldata; }
    int type() const { return m_type; }

};

#endif
