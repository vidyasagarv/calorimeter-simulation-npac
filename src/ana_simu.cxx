#include "Event.h"
#include "CaloConstants.h"
#include "TH3D.h"
#include "TH2D.h"
#include "TCanvas.h"

using namespace std;
using namespace CalConst;

void ana_simu(const Event& event)
{
  if (event.eventNumber() == 0){ // Display the shower only for the first event

    // Initialize corresponding histograms
    string tem_str = "hCalo_"+to_string(event.eventNumber());
    char const* hCalo_name = tem_str.c_str();
    TH3D * hCalo = new TH3D(hCalo_name, hCalo_name, NbCellsInXY, 0, NbCellsInXY, NbCellsInXY, 0, NbCellsInXY, NbLayers, 0, NbLayers);
    TH2D * hXYSlice[NbLayers];

    for (size_t i_z = 0; i_z < NbLayers; i_z++) {
      string tem_str_XY = "hXYSlice_"+to_string(event.eventNumber())+"_"+to_string(i_z+1);
      char const* hSlice_name = tem_str_XY.c_str();
      hXYSlice[i_z] = new TH2D(hSlice_name, "XYSlice", NbCellsInXY, 0, NbCellsInXY, NbCellsInXY, 0, NbCellsInXY);
    }

    // Set the information in histograms
    for (size_t i_c = 0; i_c < event.caldata().size(); i_c++) {
      CellAddress cl_temp = event.caldata()[i_c].address();
      hCalo->SetBinContent(cl_temp.ix()+1, cl_temp.iy()+1, cl_temp.layer()+1, event.caldata()[i_c].energy());
      hXYSlice[cl_temp.layer()]->SetBinContent(cl_temp.ix()+1, cl_temp.iy()+1, event.caldata()[i_c].energy());
    }

    hCalo->Print(); // To verify that the sum is equal to or less than 50 GeV
    hCalo->Draw();
    hCalo->Project3D("x")->Draw("");
    hCalo->Project3D("y")->Draw("");
    hCalo->Project3D("z")->Draw("");
  }
  
}
