/**
 * CellAddress
 * Implements is a way to identify a calorimter cell using its positions along
 * the x, y and z axis.
 */

#include <iostream>
#include "CellAddress.h"
#include "CaloConstants.h"

using namespace CalConst;

// Default constructor, gives you an invalid address.
CellAddress::CellAddress()
{
  m_ix = -999.;
  m_iy = -999.;
  m_layer = -999.;
}

// Full constructor
CellAddress::CellAddress(int ix, int iy, int layer)
{
  m_ix = ix;
  m_iy = iy;
  m_layer = layer;
}

// Destructor
CellAddress::~CellAddress()
{
}


// Is the address valid.
bool CellAddress::IsValid() const
{
  // Check that the coordinates are in the range of [0,Max) where the Max are defined in CaloConstants.h
  // if (x - x_min)*(x - x_max) < 0, then x in range (x_min, x_max)
  return (m_ix*(m_ix-NbCellsInXY+1) <= 0 && m_iy*(m_iy-NbCellsInXY+1) <= 0 && m_layer*(m_layer-NbLayers+1) <= 0);
}

// Accessors.
int CellAddress::ix() const {  return m_ix; }
int CellAddress::iy() const {  return m_iy; }
int CellAddress::layer() const {  return m_layer; }
